# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/).

## [0.1.0] - 2019-04-20

First public beta release!

### Fixed

- Downground okhttp to keep API 19 compataiblity
- Add null check to geo provider
- Fixed image picker crash
- Fixed tablayout styling
- Fixed support mail link

## [0.0.4] - 2019-04-11

### Added

- Add version name and extra links !40 [@dthulke](https://gitlab.com/dthulke)
- Map caching and cluster icons in Android 9 !36 [@dthulke](https://gitlab.com/dthulke)

### Fixed

- Fix unread message count #20 !38 [@dthulke](https://gitlab.com/dthulke)

### Removed

- Remove example credentials from login form #22 !39 [@dthulke](https://gitlab.com/dthulke)

## [0.0.3] - 2019-04-01

### Added

- Possibility to delete baskets #5 !31 [@alex.simm](https://gitlab.com/alex.simm).
- Zoom to current location on the map #12 !32 [@alex.simm](https://gitlab.com/alex.simm).

### Changed

- Loading all images via OkHttp and using caching #5 !31 [@alex.simm](https://gitlab.com/alex.simm).

## [0.0.2] - 2019-02-23

### Added

- nothing

## [0.0.1] - 2019-02-23

### Added

- everything
