package de.foodsharing.api

import de.foodsharing.model.ConversationDetail
import de.foodsharing.model.ConversationListEntry
import io.reactivex.Observable
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Path
import retrofit2.http.Query

/**
 * Retrofit API interface for chat messages.
 */
interface ConversationsAPI {
    /**
     * Requests a list of the user's conversations.
     */
    @GET("/api/conversations")
    fun list(): Observable<List<ConversationListEntry>>

    /**
     * Requests details and messages of a conversation.
     * @param id conversation id
     */
    @GET("/api/conversations/{id}")
    fun get(
        @Path("id") id: Int,
        @Query("messagesLimit") number: Int = 20,
        @Query("messagesOffset") offset: Int = 0
    ): Observable<ConversationDetail>

    /**
     * Attempts to send a message to all members of a conversation.
     * @param cid conversation id
     * @param body the message
     */
    @FormUrlEncoded
    @POST("/xhrapp.php?app=msg&m=sendmsg")
    fun send(
        @Field("c") cid: Int,
        @Field("b") body: String
    ): Observable<Boolean>
}