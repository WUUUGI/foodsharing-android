package de.foodsharing.api

import de.foodsharing.model.FairSharePoint
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Path

/**
 * Retrofit API interface for fair share points.
 */
interface FairSharePointAPI {

    class FairSharePointListResponse {

        var fairSharePoints: List<FairSharePoint>? = null
    }

    class FairSharePointResponse {

        var fairSharePoint: FairSharePoint? = null
    }

    /**
     * Requests a list of all food baskets of the current user.
     */
    @GET("/api/fairSharePoints?type=mine")
    fun list(): Observable<FairSharePointListResponse>

    /**
     * Requests details of a fair share point.
     */
    @GET("/api/fairSharePoints/{id}")
    fun get(@Path("id") id: Int): Observable<FairSharePointResponse>
}
