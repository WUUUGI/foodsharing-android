package de.foodsharing.model

import java.io.Serializable

/**
 * Represents a person that is participating in foodsharing.
 */
data class User(
    val id: Int,
    val email: String,
    val name: String,
    val photo: String?, // different endpoints have either photo or avatar, unify them!
    val avatar: String?
) : Serializable