package de.foodsharing.services

import com.franmontiel.persistentcookiejar.ClearableCookieJar
import de.foodsharing.api.AuthAPI
import de.foodsharing.api.LoginRequest
import de.foodsharing.api.UserResponse
import de.foodsharing.api.WebsocketAPI
import de.foodsharing.api.XhrResponse
import io.reactivex.Observable
import retrofit2.HttpException
import retrofit2.http.Body
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class AuthService @Inject constructor(
    private val api: AuthAPI,
    private val ws: WebsocketAPI,
    private val cookieJar: ClearableCookieJar
) {

    @Volatile
    var currentUser: UserResponse? = null

    fun check(): Observable<Boolean> {
        return api.current()
                .doOnNext { user -> currentUser = user }
                .map { true }
                .onErrorReturn { error ->
                    if (error is HttpException && error.code() == 404) {
                        clear()
                        false
                    } else {
                        throw(error)
                    }
                }
    }

    fun login(@Body data: LoginRequest): Observable<UserResponse> {
        return api.login(data).doOnNext { user -> currentUser = user }
    }

    fun logout(): Observable<XhrResponse> {
        return api.logout().doOnNext { clear() }
    }

    fun clear() {
        currentUser = null
        ws.close()
        cookieJar.clear()
    }
}