package de.foodsharing.ui.base

import android.annotation.SuppressLint
import android.content.IntentFilter
import android.net.ConnectivityManager
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import de.foodsharing.R
import de.foodsharing.utils.ConnectivityReceiver
import de.foodsharing.utils.Utils

@SuppressLint("Registered")
open class BaseActivity : AppCompatActivity(), ConnectivityReceiver.ConnectivityReceiverListener {

    private var snackBar: Snackbar? = null
    private var connectionSnackBar: Snackbar? = null
    var rootLayoutID: Int? = null
    var isConnected = false

    private val connectivityReceiver = ConnectivityReceiver()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        registerReceiver(
                connectivityReceiver,
                IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION)
        )
    }

    override fun onPause() {
        super.onPause()
        unregisterReceiver(connectivityReceiver)
        ConnectivityReceiver.connectivityReceiverListener = null
    }

    override fun onResume() {
        super.onResume()
        registerReceiver(
                connectivityReceiver,
                IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION)
        )
        ConnectivityReceiver.connectivityReceiverListener = this
    }

    override fun onNetworkConnectionChanged(isConnected: Boolean) {
        toggleConnectionMessage(isConnected)
        this.isConnected = isConnected
    }

    fun showMessage(message: String, duration: Int = Snackbar.LENGTH_INDEFINITE) {
        rootLayoutID?.let {
            snackBar = getSnackbar(it, message, duration)
            snackBar?.show()
        }
    }

    private fun toggleConnectionMessage(isConnected: Boolean) {
        if (!isConnected) {
            val messageToUser = getString(R.string.offline_message)
            onDisconnectedListener?.onDisconnected()
            rootLayoutID?.let {
                connectionSnackBar = getSnackbar(it, messageToUser, Snackbar.LENGTH_INDEFINITE)
                connectionSnackBar?.show()
            }
        } else {
            connectionSnackBar?.dismiss()
            onConnectedListener?.onConnectionAvailable()
        }
    }

    private fun getSnackbar(rootLayoutID: Int, messageToUser: String, duration: Int): Snackbar =
            Utils.showSnackBar(
                    findViewById(rootLayoutID),
                    messageToUser,
                    ContextCompat.getColor(this, R.color.colorAccent),
                    duration
            )

    interface OnConnectionAvailableListener {
        fun onConnectionAvailable()
    }

    interface OnDisconnectedListener {
        fun onDisconnected()
    }

    companion object {
        var onConnectedListener: OnConnectionAvailableListener? = null
        var onDisconnectedListener: OnDisconnectedListener? = null
    }
}