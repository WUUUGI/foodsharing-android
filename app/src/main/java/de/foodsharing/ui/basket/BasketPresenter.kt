package de.foodsharing.ui.basket

import de.foodsharing.api.BasketAPI
import de.foodsharing.model.Basket
import de.foodsharing.model.User
import de.foodsharing.ui.base.BasePresenter
import de.foodsharing.utils.BASE_URL
import de.foodsharing.utils.CachedResourceLoader
import de.foodsharing.utils.DEFAULT_USER_PICTURE
import de.foodsharing.utils.Utils
import javax.inject.Inject

class BasketPresenter @Inject constructor(
    private val baskets: BasketAPI,
    private val resourceLoader: CachedResourceLoader
) :
    BasePresenter<BasketContract.View>(), BasketContract.Presenter {

    override fun fetch(id: Int) {
        request(baskets.get(id)) { view?.display(it.basket!!) }
    }

    override fun loadPicture(url: String) {
        request(resourceLoader.createImageObservable("$BASE_URL/images/basket/$url")) {
            view?.displayPicture(it)
        }
    }

    override fun loadUserPicture(user: User) {
        request(
            resourceLoader.createImageObservable(
                Utils.getUserPhotoURL(user, Utils.PhotoType.MINI), DEFAULT_USER_PICTURE
            )
        ) {
            it.setHasAlpha(true)
            view?.displayUserPicture(it)
        }
    }

    override fun removeBasket(basket: Basket) {
        request(baskets.remove(basket.id)) {
            view?.basketRemoved()
        }
    }
}
