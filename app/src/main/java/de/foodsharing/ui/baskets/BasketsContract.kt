package de.foodsharing.ui.baskets

import de.foodsharing.model.Basket
import de.foodsharing.ui.base.BaseContract

class BasketsContract {

    interface View : BaseContract.View {
        fun display(baskets: List<Basket>)
    }

    interface Presenter : BaseContract.Presenter<View> {
        fun fetch()
    }
}