package de.foodsharing.ui.baskets

import de.foodsharing.api.BasketAPI
import de.foodsharing.ui.base.BasePresenter
import javax.inject.Inject

class BasketsPresenter @Inject constructor(private val baskets: BasketAPI) :
        BasePresenter<BasketsContract.View>(), BasketsContract.Presenter {

    override fun fetch() {
        request(baskets.list()) { view?.display(it.baskets!!) }
    }
}