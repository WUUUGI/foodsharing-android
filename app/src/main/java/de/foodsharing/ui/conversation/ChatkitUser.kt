package de.foodsharing.ui.conversation

import com.stfalcon.chatkit.commons.models.IUser
import de.foodsharing.model.User
import de.foodsharing.utils.Utils

class ChatkitUser(val user: User) : IUser {

    override fun getId() = user.id.toString()

    override fun getName() = user.name

    override fun getAvatar() = Utils.getUserPhotoURL(user, Utils.PhotoType.Q_130)
}
