package de.foodsharing.ui.conversations

import de.foodsharing.model.ConversationListEntry
import de.foodsharing.model.Message
import de.foodsharing.ui.base.BaseContract

class ConversationsContract {

    interface View : BaseContract.View {
        fun display(conversations: List<ConversationListEntry>)
        fun updateLatestMessage(cid: Int, message: Message)
    }

    interface Presenter : BaseContract.Presenter<View> {
        fun fetch()
    }
}