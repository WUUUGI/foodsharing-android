package de.foodsharing.ui.conversations

import de.foodsharing.api.ConversationsAPI
import de.foodsharing.api.WebsocketAPI
import de.foodsharing.model.Message
import de.foodsharing.ui.base.BasePresenter
import javax.inject.Inject

class ConversationsPresenter @Inject constructor(
    private val conversations: ConversationsAPI,
    private val ws: WebsocketAPI
) :
        BasePresenter<ConversationsContract.View>(), ConversationsContract.Presenter {

    override fun fetch() {
        request(conversations.list()) { view?.display(it) }

        request(ws.subscribe()) { msg ->
            when (msg) {
                is WebsocketAPI.ConversationMessage -> {
                    view?.updateLatestMessage(
                            msg.cid,
                            Message(msg.id, msg.fsId, msg.fsName, msg.body, msg.time)
                    )
                }
            }
        }
    }
}