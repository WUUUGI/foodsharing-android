package de.foodsharing.ui.fairsharepoint

import android.graphics.Bitmap
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import de.foodsharing.R
import de.foodsharing.di.Injectable
import de.foodsharing.model.FairSharePoint
import de.foodsharing.services.AuthService
import de.foodsharing.ui.base.BaseActivity
import de.foodsharing.utils.CachedResourceLoader
import kotlinx.android.synthetic.main.activity_fairsharepoint.*
import javax.inject.Inject

class FairSharePointActivity : BaseActivity(), FairSharePointContract.View, Injectable {

    @Inject
    lateinit var presenter: FairSharePointContract.Presenter

    @Inject
    lateinit var resourceLoader: CachedResourceLoader

    @Inject
    lateinit var auth: AuthService

    private var fairSharePoint: FairSharePoint? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        presenter.attach(this)
        setContentView(R.layout.activity_fairsharepoint)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        // TODO: Add constant key for extra
        if (intent.hasExtra("fairSharePoint")) {
            fairSharePoint = intent.getSerializableExtra("fairSharePoint") as FairSharePoint
            fairSharePoint?.let {
                supportActionBar?.title =
                    "${applicationContext.getString(R.string.fair_share_point_toolbar)}: #${it.id}"
                display(it)
            }
        } else {
            val id = intent.getIntExtra("id", -1)
            supportActionBar?.title = "${applicationContext.getString(R.string.fair_share_point_toolbar)}: #$id"
            presenter.fetch(id)
        }
    }

    // TODO: Check if user is creater of this fair share point(or is allowed), if true add menu item to delete this fair share point
    override fun onOptionsItemSelected(item: MenuItem): Boolean = when (item.itemId) {
        android.R.id.home -> {
            onBackPressed()
            true
        }
        else -> super.onOptionsItemSelected(item)
    }

    override fun finish() {
        super.finish()
        overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right)
    }

    override fun onDestroy() {
        presenter.unsubscribe()
        super.onDestroy()
    }

    override fun display(fairSharePoint: FairSharePoint) {
        this.fairSharePoint = fairSharePoint
        findViewById<TextView>(R.id.fair_share_point_name).text = fairSharePoint.id.toString()
        findViewById<TextView>(R.id.fair_share_point_description).text = "Test"
        findViewById<TextView>(R.id.fair_share_point_address).text =
            "Lat: ${fairSharePoint.lat}\nLon: ${fairSharePoint.lon}"

        invalidateOptionsMenu()

        basket_content_view.visibility = View.VISIBLE
        progress_bar.visibility = View.GONE

        // TODO: Check response from REST call
//
//        if (fairSharePoint.picture.isNullOrEmpty()) findViewById<ImageView>(R.id.basket_picture).visibility = View.GONE
//        else fairSharePoint.picture?.let { presenter.loadPicture(it) }
//
//        presenter.loadUserPicture(basket.creator)
    }

    override fun displayPicture(picture: Bitmap) {
        findViewById<ImageView>(R.id.fair_share_point_picture).setImageBitmap(picture)
    }
}