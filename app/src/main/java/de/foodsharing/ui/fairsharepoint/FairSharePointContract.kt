package de.foodsharing.ui.fairsharepoint

import android.graphics.Bitmap
import de.foodsharing.model.FairSharePoint
import de.foodsharing.ui.base.BaseContract

class FairSharePointContract {

    interface View : BaseContract.View {
        fun display(fairSharePoint: FairSharePoint)
        fun displayPicture(picture: Bitmap)
    }

    interface Presenter : BaseContract.Presenter<View> {
        fun fetch(id: Int)
        fun loadPicture(url: String)
    }
}