package de.foodsharing.ui.fairsharepoint

import de.foodsharing.api.FairSharePointAPI
import de.foodsharing.ui.base.BasePresenter
import de.foodsharing.utils.BASE_URL
import de.foodsharing.utils.CachedResourceLoader
import javax.inject.Inject

class FairSharePointPresenter @Inject constructor(
    private val fairSharePoints: FairSharePointAPI,
    private val resourceLoader: CachedResourceLoader
) :
    BasePresenter<FairSharePointContract.View>(), FairSharePointContract.Presenter {

    override fun fetch(id: Int) {
        request(fairSharePoints.get(id)) { view?.display(it.fairSharePoint!!) }
    }

    override fun loadPicture(url: String) {
        request(resourceLoader.createImageObservable("$BASE_URL/images/fairSharePoints/$url")) {
            view?.displayPicture(it)
        }
    }
}