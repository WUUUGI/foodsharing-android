package de.foodsharing.ui.initial

import android.content.Intent
import android.os.Bundle
import com.franmontiel.persistentcookiejar.ClearableCookieJar
import de.foodsharing.di.Injectable
import de.foodsharing.services.AuthService
import de.foodsharing.ui.base.BaseActivity
import de.foodsharing.ui.login.LoginActivity
import de.foodsharing.ui.main.MainActivity
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class InitialActivity : BaseActivity(), Injectable {

    @Inject
    lateinit var auth: AuthService

    @Inject
    lateinit var cookieJar: ClearableCookieJar

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        auth.check()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { isLoggedIn ->
                    startActivity(Intent(
                            this,
                            if (isLoggedIn) MainActivity::class.java else LoginActivity::class.java
                    ))
                    finish()
                }
    }
}
