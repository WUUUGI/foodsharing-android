package de.foodsharing.ui.login

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.text.SpannableString
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.view.View
import de.foodsharing.BuildConfig
import de.foodsharing.R
import de.foodsharing.di.Injectable
import de.foodsharing.ui.base.BaseActivity
import de.foodsharing.ui.main.MainActivity
import de.foodsharing.utils.Utils
import kotlinx.android.synthetic.main.activity_login.login_button
import kotlinx.android.synthetic.main.activity_login.password_field
import kotlinx.android.synthetic.main.activity_login.progress_bar
import kotlinx.android.synthetic.main.activity_login.register_button
import kotlinx.android.synthetic.main.activity_login.user_field
import kotlinx.android.synthetic.main.activity_login.version_text_view
import retrofit2.HttpException
import javax.inject.Inject

class LoginActivity : BaseActivity(), LoginContract.View,
        BaseActivity.OnConnectionAvailableListener, Injectable {

    @Inject
    lateinit var presenter: LoginContract.Presenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        rootLayoutID = R.id.root
        onConnectedListener = this
        presenter.attach(this)

        login_button.setOnClickListener {
            if (isConnected) {
                showProgress(true)
                presenter.login(user_field.text.toString(), password_field.text.toString())
            }
        }

        register_button.setOnClickListener {
            val intent = Intent(Intent.ACTION_VIEW)
            intent.data = Uri.parse("https://foodsharing.de/?page=content&sub=joininfo")
            startActivity(intent)
        }

        setupVersionText()
    }

    fun setupVersionText() {
        val version = getString(R.string.version, BuildConfig.VERSION_NAME)
        val reportIssue = getString(R.string.report_login_issue)
        val versionText = SpannableString("$version - $reportIssue")
        versionText.setSpan(object : ClickableSpan() {
            override fun onClick(widget: View) {
                Utils.openSupportEmail(this@LoginActivity)
            }
        }, versionText.length - reportIssue.length, versionText.length, 0)
        version_text_view.movementMethod = LinkMovementMethod.getInstance()

        version_text_view.text = versionText
    }

    override fun showProgress(show: Boolean) {
        progress_bar.visibility = if (show) {
            View.VISIBLE
        } else {
            View.GONE
        }
    }

    override fun loginResult(result: Boolean, userId: Int?) {
        showProgress(false)
        if (result) {
            startActivity(Intent(this, MainActivity::class.java))
            finish()
        } else {
            showErrorMessage(getString(R.string.invalid_password))
        }
    }

    override fun showErrorMessage(error: String) {
        showProgress(false)
        showMessage(error, duration = Snackbar.LENGTH_LONG)
    }

    override fun showErrorMessage(error: Throwable) {
        if (error is HttpException) {
            when (error.code()) {
                401 -> showErrorMessage(getString(R.string.invalid_password))
            }
        } else {
            showErrorMessage(error.localizedMessage)
        }
    }

    override fun onConnectionAvailable() {
    }

    override fun onDestroy() {
        presenter.unsubscribe()
        onConnectedListener = null
        super.onDestroy()
    }
}
