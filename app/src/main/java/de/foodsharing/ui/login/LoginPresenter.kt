package de.foodsharing.ui.login

import android.util.Log
import de.foodsharing.api.LoginRequest
import de.foodsharing.services.AuthService
import de.foodsharing.ui.base.BasePresenter
import de.foodsharing.utils.LOG_TAG
import javax.inject.Inject

class LoginPresenter @Inject constructor(private val auth: AuthService) : BasePresenter<LoginContract.View>(), LoginContract.Presenter {

    override fun login(user: String, password: String) {
        request(auth.login(LoginRequest(user, password, true)), { user ->
            Log.v(LOG_TAG, "Logged in as ${user.id} ${user.name}")
            view?.loginResult(true, user.id)
        }, { error ->
            view?.showErrorMessage(error)
        })
    }
}