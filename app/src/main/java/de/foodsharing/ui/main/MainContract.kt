package de.foodsharing.ui.main

import de.foodsharing.ui.base.BaseContract

class MainContract {

    interface View : BaseContract.View

    interface Presenter : BaseContract.Presenter<View> {
        /**
         * Logs out the user and closes the chat socket.
         */
        fun logout()
    }
}