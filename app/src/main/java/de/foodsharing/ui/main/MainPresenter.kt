package de.foodsharing.ui.main

import de.foodsharing.services.AuthService
import de.foodsharing.ui.base.BasePresenter
import javax.inject.Inject

class MainPresenter @Inject constructor(
    private val auth: AuthService
) : BasePresenter<MainContract.View>(), MainContract.Presenter {

    override fun logout() {
        request(auth.logout())
    }
}