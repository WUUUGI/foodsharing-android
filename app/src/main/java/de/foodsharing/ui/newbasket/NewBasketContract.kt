package de.foodsharing.ui.newbasket

import de.foodsharing.model.Basket
import de.foodsharing.ui.base.BaseContract
import java.io.File

class NewBasketContract {

    interface View : BaseContract.View {

        /**
         * Called after a basket has been published.
         */
        fun display(basket: Basket)

        /**
         * Called if publishing failed.
         */
        fun showError(message: String)
    }

    interface Presenter : BaseContract.Presenter<View> {

        fun publish(
            description: String,
            phone: String?,
            mobile: String?,
            contactByMessage: Boolean,
            weight: Float,
            lifetime: Int,
            location: Pair<Double, Double>?,
            picture: File?
        )
    }
}