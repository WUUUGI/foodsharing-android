package de.foodsharing.ui.picture

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import de.foodsharing.R
import de.foodsharing.ui.base.BaseActivity
import de.foodsharing.utils.Utils
import kotlinx.android.synthetic.main.activity_picture.picture_activity_view
import kotlinx.android.synthetic.main.activity_picture.picture_toolbar

class PictureActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_picture)
        setSupportActionBar(picture_toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = ""
    }

    override fun onWindowFocusChanged(hasFocus: Boolean) {
        super.onWindowFocusChanged(hasFocus)

        // load and show rescaled picture
        if (hasFocus && intent.hasExtra("pictureFile")) {
            val picture = Utils.loadRescaledBitmap(
                    intent.getStringExtra("pictureFile"),
                    picture_activity_view.width,
                    picture_activity_view.height
            )
            picture_activity_view.setImageBitmap(picture)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.picture_menu, menu)
        return true
    }

    override fun onBackPressed() {
        setResult(RESULT_OK)
        super.onBackPressed()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean = when (item.itemId) {
        android.R.id.home -> {
            onBackPressed()
            true
        }
        R.id.picture_button_remove -> {
            setResult(RESULT_CANCELED)
            finish()
            true
        }
        else -> super.onOptionsItemSelected(item)
    }

    override fun finish() {
        super.finish()
        overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right)
    }
}
