package de.foodsharing.utils

import com.franmontiel.persistentcookiejar.ClearableCookieJar
import okhttp3.Interceptor
import okhttp3.Response
import javax.inject.Inject

const val CSRF_COOKIE_NAME = "CSRF_TOKEN"
const val CSRF_HEADER_NAME = "X-CSRF-Token"

class CSRFInterceptor @Inject constructor(private val cookieJar: ClearableCookieJar) : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        var request = chain.request()
        val csrfCookie = cookieJar.loadForRequest(request.url()).find { it.name() == CSRF_COOKIE_NAME }
        csrfCookie?.let {
            request = request.newBuilder().addHeader(CSRF_HEADER_NAME, it.value()).build()
        }
        return chain.proceed(request)
    }
}