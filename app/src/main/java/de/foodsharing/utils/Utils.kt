package de.foodsharing.utils

import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Environment
import android.support.design.widget.Snackbar
import android.view.View
import de.foodsharing.R
import de.foodsharing.model.User
import java.io.File
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.Date

object Utils {

    /**
     * Creates a [Snackbar] via [Snackbar.make] and sets its background color. The duration will
     * be [Snackbar.LENGTH_SHORT].
     *
     * @param view The view to find a parent from.
     * @param text The text to show. Can be formatted text.
     * @param backgroundColor The background color.
     *
     * @return the created Snackbar
     */
    fun showSnackBar(view: View, text: String, backgroundColor: Int): Snackbar =
        Snackbar.make(view, text, Snackbar.LENGTH_SHORT).withColor(backgroundColor)

    /**
     * Creates a [Snackbar] via [Snackbar.make] and sets its background color.
     *
     * @param view The view to find a parent from.
     * @param text The text to show. Can be formatted text.
     * @param backgroundColor The background color.
     * @param duration How long to display the message. Either [Snackbar.LENGTH_SHORT] or
     * [Snackbar.LENGTH_LONG].
     *
     * @return the created Snackbar
     */
    fun showSnackBar(view: View, text: String, backgroundColor: Int, duration: Int): Snackbar {
        return Snackbar.make(
                view,
                text,
                duration
        ).withColor(backgroundColor)
    }

    /**
     * Shows a yes-no-dialog with a specific message and calls the result function afterwards.
     */
    fun showQuestionDialog(context: Context, message: String, result: (Boolean) -> Unit) {
        val listener = DialogInterface.OnClickListener { dialog, which ->
                dialog.dismiss()
                result(which == DialogInterface.BUTTON_POSITIVE)
        }

        AlertDialog.Builder(context)
                .setMessage(message)
                .setPositiveButton(android.R.string.yes, listener)
                .setNegativeButton(android.R.string.no, listener)
                .show()
    }

    enum class PhotoType(val prefix: String) {
        NORMAL(""), CROP("crop_"), THUMB_CROP("thumb_crop_"),
        Q_130("130_q_"), MINI("mini_q_")
    }

    fun getUserPhotoURL(user: User, type: PhotoType = Utils.PhotoType.Q_130): String {
        val filename = user.photo ?: user.avatar
        return if (filename != null && filename.isNotEmpty()) {
            "$BASE_URL/images/${type.prefix}$filename"
        } else DEFAULT_USER_PICTURE
    }

    @Throws(IOException::class)
    fun createImageFile(context: Context): File {
        val timeStamp = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
        val storageDir = context.getExternalFilesDir(Environment.DIRECTORY_PICTURES)
        return File.createTempFile(
                "${PICTURE_FORMAT.toUpperCase()}_${timeStamp}_",
                ".$PICTURE_FORMAT", storageDir
        )
    }

    /**
     * Loads a bitmap from a file and rescales it to a specific size.
     */
    fun loadRescaledBitmap(filePath: String, width: Int = 0, height: Int = 0): Bitmap {
        val options = BitmapFactory.Options().apply {
            // Get the dimensions of the bitmap
            inJustDecodeBounds = true
            BitmapFactory.decodeFile(filePath, this)
            val photoW = outWidth
            val photoH = outHeight

            // Determine how much to scale down the image
            inSampleSize =
                    if (width == 0 || height == 0) 1 else Math.min(photoW / width, photoH / height)
            inJustDecodeBounds = false
            inPurgeable = true
        }
        return BitmapFactory.decodeFile(filePath, options)
    }

    fun openSupportEmail(context: Context) {
        val intent = Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                "mailto", context.getString(R.string.support_email), null))
        context.startActivity(Intent.createChooser(intent, context.getString(R.string.send_email_title)))
    }
}