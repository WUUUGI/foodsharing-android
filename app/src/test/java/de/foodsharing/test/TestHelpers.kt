package de.foodsharing.test

import de.foodsharing.model.Basket
import de.foodsharing.model.FairSharePoint
import de.foodsharing.model.User
import io.reactivex.android.plugins.RxAndroidPlugins
import io.reactivex.plugins.RxJavaPlugins
import io.reactivex.schedulers.Schedulers
import java.util.Date
import kotlin.random.Random

fun configureTestSchedulers() {
    // See https://medium.com/@dbottillo/how-to-unit-test-your-rxjava-code-in-kotlin-d239364687c9
    RxJavaPlugins.setIoSchedulerHandler { Schedulers.trampoline() }
    RxJavaPlugins.setComputationSchedulerHandler { Schedulers.trampoline() }
    RxJavaPlugins.setNewThreadSchedulerHandler { Schedulers.trampoline() }
    RxAndroidPlugins.setInitMainThreadSchedulerHandler { Schedulers.trampoline() }
}

fun createRandomUser(): User {
    val id = Random.nextInt()
    return User(id, "user$id@foo.com", "user$id", null, null)
}

fun createRandomBasket(creator: User = createRandomUser()) = Basket(
    Random.nextInt(),
    "description",
    null,
    Date(),
    Date(),
    arrayOf(1, 2),
    Date(),
    Random.nextDouble(-90.0, 90.0),
    Random.nextDouble(-180.0, 180.0),
    creator
)

fun createRandomFairSharePoint() = FairSharePoint(
    Random.nextInt(),
    Random.nextInt(),
    Random.nextDouble(-90.0, 90.0),
    Random.nextDouble(-180.0, 180.0)
)