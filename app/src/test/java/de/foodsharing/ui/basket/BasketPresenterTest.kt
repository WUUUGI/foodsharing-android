package de.foodsharing.ui.basket

import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.verifyNoMoreInteractions
import com.nhaarman.mockitokotlin2.whenever
import de.foodsharing.api.BasketAPI
import de.foodsharing.test.configureTestSchedulers
import de.foodsharing.test.createRandomBasket
import de.foodsharing.utils.CachedResourceLoader
import io.reactivex.Observable.just
import okhttp3.OkHttpClient
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.mockito.ArgumentMatchers.anyInt
import org.mockito.Mock
import org.mockito.MockitoAnnotations

class BasketPresenterTest {

    lateinit var presenter: BasketPresenter

    @Mock
    lateinit var view: BasketContract.View

    @Mock
    lateinit var baskets: BasketAPI

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
        configureTestSchedulers()
        presenter = BasketPresenter(baskets, CachedResourceLoader(OkHttpClient.Builder().build()))
        presenter.attach(view)
    }

    @Test
    fun `fetch basket`() {
        val basket = createRandomBasket()
        whenever(baskets.get(anyInt())) doReturn just(BasketAPI.BasketResponse().apply {
            this.basket = basket
        })
        presenter.fetch(basket.id)
        verify(view).display(basket)
    }

    @After
    fun after() {
        verifyNoMoreInteractions(view)
    }
}
