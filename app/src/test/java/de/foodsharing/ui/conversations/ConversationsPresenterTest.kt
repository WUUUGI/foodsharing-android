package de.foodsharing.ui.conversations

import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.verifyNoMoreInteractions
import com.nhaarman.mockitokotlin2.whenever
import de.foodsharing.api.ConversationsAPI
import de.foodsharing.api.WebsocketAPI
import de.foodsharing.model.ConversationListEntry
import de.foodsharing.test.configureTestSchedulers
import de.foodsharing.test.createRandomUser
import io.reactivex.Observable
import io.reactivex.Observable.just
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.MockitoAnnotations
import java.text.SimpleDateFormat
import java.util.Date
import kotlin.random.Random

class ConversationsPresenterTest {

    lateinit var presenter: ConversationsPresenter

    @Mock
    lateinit var view: ConversationsContract.View

    @Mock
    lateinit var api: ConversationsAPI

    @Mock
    lateinit var ws: WebsocketAPI

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
        configureTestSchedulers()
        presenter = ConversationsPresenter(api, ws)
        presenter.attach(view)
    }

    @Test
    fun `fetch conversations`() {
        val currentUser = createRandomUser()

        val conversations = mutableListOf<ConversationListEntry>()
        for (i in 1..10) {
            val conversationId = Random.nextInt()
            val last = Date()
            val lastTSString = SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(last)
            val user = createRandomUser()

            conversations.add(
                ConversationListEntry(
                    conversationId,
                    lastTSString,
                    user.id,
                    "last message",
                    last,
                    null,
                    Random.nextInt(2),
                    listOf(currentUser, user)
                )
            )
            print(conversations.get(conversations.size - 1))
        }

        whenever(api.list()) doReturn just(conversations as List<ConversationListEntry>)
        whenever(ws.subscribe()) doReturn Observable.create { }
        presenter.fetch()
        verify(view).display(conversations)
    }

    @After
    fun after() {
        verifyNoMoreInteractions(view)
    }
}