package de.foodsharing.ui.map

import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.verifyNoMoreInteractions
import com.nhaarman.mockitokotlin2.whenever
import de.foodsharing.api.MapAPI
import de.foodsharing.model.Basket
import de.foodsharing.model.FairSharePoint
import de.foodsharing.test.configureTestSchedulers
import de.foodsharing.test.createRandomBasket
import de.foodsharing.test.createRandomFairSharePoint
import io.reactivex.Observable.just
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.MockitoAnnotations

class MapPresenterTest {

    lateinit var presenter: MapPresenter

    @Mock
    lateinit var view: MapContract.View

    @Mock
    lateinit var api: MapAPI

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
        configureTestSchedulers()
        presenter = MapPresenter(api)
        presenter.attach(view)
    }

    @Test
    fun `fetch markers`() {
        val fairteiler = mutableListOf<FairSharePoint>()
        for (i in 1..10) {
            fairteiler.add(createRandomFairSharePoint())
        }
        val baskets = mutableListOf<Basket>()
        for (i in 1..10) {
            baskets.add(createRandomBasket())
        }

        val response = MapAPI.MapResponse(1, fairteiler, baskets)

        whenever(api.coordinates()) doReturn just(response)
        presenter.fetch()
        verify(view).setMarkers(fairteiler, baskets)
    }

    @After
    fun after() {
        verifyNoMoreInteractions(view)
    }
}
